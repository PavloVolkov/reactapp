import { expect } from '@playwright/test';
export class NavigationPage {
    constructor(page) {
        this.page = page;
        this.getBasketCounter = page.locator('[data-qa="header-basket-count"]');
        this.checkoutBtn = page.getByRole('link', { name: 'Checkout' });
    }

    async basketCounterHasText(value) {
        const counter = this.getBasketCounter;
        await expect(counter).toHaveText(value.toString());
    }

    async checkoutBtnClick() {
        const checkoutBtn = this.checkoutBtn;
        await checkoutBtn.waitFor();
        await checkoutBtn.click();
        await this.page.waitForURL(`http://localhost:2221/basket`);
    }
}
