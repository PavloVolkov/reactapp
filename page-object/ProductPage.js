import { expect } from '@playwright/test';
import { NavigationPage } from './Navigation';
export class ProductPage {
    constructor(page) {
        this.page = page;
        this.getAddToBasketBtn = page.locator('[data-qa="product-button"]');
    }
    async visit(additionalURL = '') {
        await this.page.goto(`http://localhost:2221/${additionalURL}`);
        await this.page.waitForURL(`http://localhost:2221/${additionalURL}`);
    }
    async addItem(itemNumber) {
        const navigation = new NavigationPage(this.page);
        const card = this.getAddToBasketBtn.nth(itemNumber);

        await card.waitFor();
        await expect(card).toHaveText('Add to Basket');
        await card.click();

        await expect(card).toHaveText('Remove from Basket');
        await navigation.basketCounterHasText(itemNumber + 1);
    }
}
