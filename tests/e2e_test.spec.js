import { test, expect } from '@playwright/test';
import { ProductPage } from '../page-object/ProductPage';
import { NavigationPage } from '../page-object/Navigation';

test.only('first e2e', async ({ page }) => {
    const productPage = new ProductPage(page);
    const navigationPage = new NavigationPage(page);
    await productPage.visit();

    await productPage.addItem(0);
    await productPage.addItem(1);
    await productPage.addItem(2);

    await navigationPage.checkoutBtnClick();
    await page.pause();
});
