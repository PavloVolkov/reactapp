import { test, expect } from '@playwright/experimental-ct-react';
import ButtonCustom from '../src/components/button.js';

// test.use({ viewport: { width: 500, height: 500 } });

test('should have text', async ({ mount }) => {
    const component = await mount(<ButtonCustom />);
    await expect(component).toContainText('Hi, I am button');
});
